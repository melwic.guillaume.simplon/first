package co.simplon.promo16.oop;

import java.util.ArrayList;
import java.util.List;

public class NameOrganizer {
    private List<String> stringList = new ArrayList<>(
            List.of("Yousra", "Phonevilay", "Florian", "Roland", "Melissa", "Florent",
                    "Magatte", "Melwic", "Aïcha", "Oualid", "Claire", "Kevin", "Khaled", "Rafaël", "Nolwenn"));

    public List<Person> promoMaker() {
        List<Person> promoList = new ArrayList<>();
        for (String firstName : stringList) {
            Person person = new Person("P16", firstName);
            promoList.add(person);
        }
        return promoList;
    }

    /**
     * public void getNameStartingBy(String letter) {
     * List<String> stringsList2 = new ArrayList<>();
     * for (String firstName : stringList) {
     * if (firstName.startsWith(letter.toUpperCase())) {
     * stringsList2.add(firstName);
     * }
     * }
     * System.out.println(stringsList2);
     * }
     * 
     * public void oddOnly() {
     * for (int i = 0; i < stringList.size(); i += 2) {
     * System.out.println(stringList.get(i));
     * }
     * }
     * 
     * public void StudentS() {
     * for (int i = 0; i < stringList.size(); i++) {
     * if (i == 0) {
     * System.out.println(stringList.get(i).toUpperCase());
     * }
     * if (i == stringList.size() - 1) {
     * System.out.println(stringList.get(i).toUpperCase());
     * }
     * }
     * }
     **/
}