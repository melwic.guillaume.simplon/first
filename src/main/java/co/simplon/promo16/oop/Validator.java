package co.simplon.promo16.oop;

public class Validator {
    public int max;
    public int min;

    public Boolean isCorrectLength(String str) {
        if (str.length() > min && str.length() < max)
            return true;
            return false;
    }

    public Boolean haveTrailingSpaces(String str) {
        if (str.startsWith(" ") || str.endsWith(" "))
            return true;
            return false;
    }

    public Boolean haveSpecialChars(String str) {
        if (str.contains("-") || str.contains("!") || str.contains("_"))
            return true;
            return false;
    }

    public Boolean validate(String str) {
        return isCorrectLength(str) && !haveSpecialChars(str) && !haveTrailingSpaces(str);
    }
}