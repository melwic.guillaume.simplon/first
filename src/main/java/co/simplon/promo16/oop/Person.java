package co.simplon.promo16.oop;

public class Person {
    public String name;
    public String firstName;
    public int energy = 65;

    public Person(String name, String firstName) {
        this.name = name;
        this.firstName = firstName;
    }

    public String toString() {
         return "This is a Person. name: " + name + ", firstName: " + firstName + "and energy: "+energy;
         }

    public void eat(Food food) {
        System.out.println(firstName + " eats " + food.getName());
        energy += food.getCalories() / 10;
        System.out.println(energy);
    }
    /**
     * }
     * public void sleep() {
     * if (energy<=60)
     * energy+=40;
     * }
     * 
     * 
     * public void socialize(Person person){
     * System.out.println("Hello "+person.firstName+", how are you ?");
     * energy-=10;
     * person.energy-=10;
     * System.out.println("Energy : "+firstName+" "+ energy);
     * System.out.println("Energy : "+person.firstName+" "+ person.energy);
     * if (person.energy>=70)
     * System.out.println("Hello "+firstName+ ", I'm fine");
     * if (person.energy<70 && person.energy>40)
     * System.out.println("Hello "+firstName+ ", I'm ok");
     * if (person.energy<=40 && person.energy>=10)
     * System.out.println("Hello "+firstName+ ", I can't even");
     * if (person.energy<10)
     * System.out.println("*"+person.firstName+" roll on the floor in foetus
     * position*");
     * }
     */
}